---------------------------------------drill 1-------------------------------------------------------------------------


pawan@pawan_pc MINGW64 ~
$ mkdir hello

pawan@pawan_pc MINGW64 ~
$ cd hello

pawan@pawan_pc MINGW64 ~/hello
$ mkdir five

pawan@pawan_pc MINGW64 ~/hello
$ cd five

pawan@pawan_pc MINGW64 ~/hello/five
$ mkdir six

pawan@pawan_pc MINGW64 ~/hello/five
$ cd six

pawan@pawan_pc MINGW64 ~/hello/five/six
$ touch c.txt

pawan@pawan_pc MINGW64 ~/hello/five/six
$ mkdir seven

pawan@pawan_pc MINGW64 ~/hello/five/six
$ cd seven

pawan@pawan_pc MINGW64 ~/hello/five/six/seven
$ touch error.log

pawan@pawan_pc MINGW64 ~/hello/five/six/seven
$ cd ../../../

pawan@pawan_pc MINGW64 ~/hello
$ mkdir one

pawan@pawan_pc MINGW64 ~/hello
$ cd one

pawan@pawan_pc MINGW64 ~/hello/one
$ touch a.txt

pawan@pawan_pc MINGW64 ~/hello/one
$ touch b.txt

pawan@pawan_pc MINGW64 ~/hello/one
$ mkdir two

pawan@pawan_pc MINGW64 ~/hello/one
$ cd two

pawan@pawan_pc MINGW64 ~/hello/one/two
$ touch d.txt

pawan@pawan_pc MINGW64 ~/hello/one/two
$ mkdir three

pawan@pawan_pc MINGW64 ~/hello/one/two
$ cd three

pawan@pawan_pc MINGW64 ~/hello/one/two/three
$ touch e.txt

pawan@pawan_pc MINGW64 ~/hello/one/two/three
$ mkdir four

pawan@pawan_pc MINGW64 ~/hello/one/two/three
$ cd four

pawan@pawan_pc MINGW64 ~/hello/one/two/three/four
$ touch access.log


pawan@pawan_pc MINGW64 ~/hello/one/two/three/four
$ cd ..

pawan@pawan_pc MINGW64 ~/hello/one/two/three
$ cd ../../../

pawan@pawan_pc MINGW64 ~/hello
$ find . -type f -iname \*.log
./five/six/seven/error.log
./one/two/three/four/access.log

pawan@pawan_pc MINGW64 ~/hello
$ find . -type f -iname \*.log -delete

pawan@pawan_pc MINGW64 ~/hello
$ cd one

pawan@pawan_pc MINGW64 ~/hello/one
$ cat > a.txt
Unix is a family of multitasking, multiuser computer operating systems that derive from the original AT&T Unix, development starting in the 1970s at the Bell Labs research center by Ken Thompson, Dennis Ritchie, and others.

pawan@pawan_pc MINGW64 ~/hello/one
$ cat a.txt
Unix is a family of multitasking, multiuser computer operating systems that derive from the original AT&T Unix, development starting in the 1970s at the Bell Labs research center by Ken Thompson, Dennis Ritchie, and others.

pawan@pawan_pc MINGW64 ~/hello/one
$ cd ..

pawan@pawan_pc MINGW64 ~/hello
$ rm -r five



--------------------------Drill 2 / Harry Potter-----------------------------------------------------------------------------




$wget "https://raw.githubusercontent.com/bobdeng/owlreader/master/ERead/assets/books/Harry%20Potter%20and%20the%20Goblet%20of%20Fire.txt"

$head -3  Harry_potter.txt //print first 3 lines

$tail -10  Harry_potter.txt //print last 10 lines

$grep -o "Harry" Harry_potter.txt | wc -l  //will print the count of a number in every line

$grep -o "Ron" Harry_potter.txt | wc -l

$grep -o "Hermione" Harry_potter.txt | wc -l

$grep -o "Dumbledore" Harry_potter.txt | wc -l

$sed -n '100,200p' Harry_potter.txt  //will print the line starts from 100 to 200

$sort Harry_potter.txt | uniq | wc -l  //will print the number of unique words
